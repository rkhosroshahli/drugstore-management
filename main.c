#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct drug{
    char name[20];
    char expire_date[10];
    char product_date[10];
    int price;
    char license[3];
    int stock;
    int n;
}medicine[50];

void add()
{    
    FILE *f;
    f=fopen("text file.text","a");
    if (f==NULL)
    {
        printf("there is not a file");
        exit(1);
    }
    else 
    {
    int i;
    int s;
    for(i=0;i<50;i++)
    {
        if (medicine[i].n==0)
        {
           medicine[i].n++;
           s=i;
            break;
        }
    }
    printf("name :\n");
    getchar();
    scanf("%s",medicine[s].name);
    printf("product date :\n");
    scanf ("%s",medicine[s].product_date);
    printf("expire date :\n");
    scanf("%s",medicine[s].expire_date);
    printf("price :\n");
    scanf("%d",&medicine[s].price);
    printf("recipe :(yes or no)\n");
    scanf("%s",medicine[s].license);
    printf("stock :\n");
    scanf("%d",&medicine[s].stock);
    fwrite(&medicine,sizeof(medicine),1,f);
    }
    fclose(f);
}

 void search()
{
    FILE *f=fopen("text file.text","r+");
    fread(&medicine,sizeof(medicine),50,f);
    if(f=NULL)
    {
        printf("this file cannot open");
        exit(1);
    }
    else
    {
    char c[20];
    printf("Which drug do you want to search?\n");
    getchar();
    scanf("%s",c);
    int k=0;
    for(int i=0;i<50;i++)
    {
        if (strcmp(c,medicine[i].name)==0)
        {
            printf("the drug with name %s with price %d has amount of %d with product date:%s and expire date:%s",medicine[i].name,medicine[i].price,medicine[i].stock,medicine[i].product_date,medicine[i].expire_date);
            if(strcmp(medicine[i].license,"yes")==0)
            {
                printf("\nit must have recipe");
            }
            break;
        }
        else
           {
            k++;
           }
       
    }
    if (k==50)
    {
        printf("there is not a drug with that name :");
    }
    }
    fclose(f);
}
void clear()
{
    FILE *f=fopen("text file.text","w+");
    fread(&medicine,50*sizeof(medicine),50,f);
    if(f=NULL)
    {
        printf("this file cannot open");
        exit(1);
    }
    else
    {
    char c[20];
    printf("which one of drug do you want to delete?\n");
    scanf("%s",c);
    int k=0;
    for(int i=0;i<50;i++)
    {
        if(strcmp(c,medicine[i].name)==0)
        {
            for(int j=i;j<49;j++)
            {
            medicine[j]=medicine[j+1];
            k=1;
            }
        }
        
    }
    if(k==0)
    {
      printf("there is not a drug with that name"); 
    }
    }
    fclose(f);
}
void increase()
{
    FILE *f=fopen("text file.text","r+");
    fread(&medicine,sizeof(medicine),50,f);
    if(f=NULL)
    {
        printf("this file cannot open");
        exit(1);
    }
    else
    {
    char c[20];
    int i;
    int n;
    printf("which drug do you want to increase it's count?");
    getchar();
    fgets(c,20,stdin);
    printf("how much do you want to increase?");
    scanf("%d",&n);
    int k=0;
    for(i=0;i<50;i++)
    {
        if(strcmp(medicine[i].name,c)==0)
        {
            medicine[i].stock+=n;
            k++;
        }
    }
    if(k==0)
    {
        
            printf("there is not also a drug with that name");
         
    }
    }
    fclose(f);
}
void sell()
{
    FILE *f=fopen("text file.text","r+");
    fread(&medicine,sizeof(medicine),50,f);
    if(f=NULL)
    {
        printf("this file cannot open");
        exit(1);
    }
    else
    {
    char c[20];
    int n;
    int i;
    int k=0;
    printf("enter name of drug to sell ( if you type 'end' it will finish");
    getchar();
        fgets(c,20,stdin);
        for(i=0;i<50;i++)
        {
            if(strcmp(medicine[i].name,c)==0)
            {
                printf("how many do you want to buy?");
                scanf("%d",&n);
                k++;
                medicine[i].stock-=n;
                
                if(n>medicine[i].stock)
                {
                    printf("there is not enough amount");
                }
            }
            
        }
        if(k==0)
        {
                printf("there is no drug with that name");
        }
    }
        fclose(f);
}
void main()
{
    int n;
    while(n<6||n>0)
    {
        printf("Please choose your action:1.Add a new drug 2.Search drug 3.Clear drug 4.increase stock 5.selling\n(please choose number)");
    scanf("%d",&n); 
    switch(n)
    {
        case 1:{add();break;}
        case 2:{search();break;}
        case 3:{clear();break;}
        case 4:{increase();break;}
        case 5:{sell();break;}
        default:{printf("Please choose a number again\n");break;}
    }
    }
}